﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public enum PlacementState
{
    Normal,
    Overlap,
    Overwrite
}

/// <summary> 
/// Provides an editor for the <see cref="TileMap"/> component
/// </summary>
[CustomEditor(typeof(TileMap))]
public class TileMapEditor : Editor
{
    public static HashSet<TileMapEditor> editors = new HashSet<TileMapEditor>();

    public GameObject tileObj = null;
    public PlacementState placement = PlacementState.Normal;

    private Vector3 mouseHitPos;

    // Unity Event Methods
    private void OnSceneGUI()
    {
        if (UpdateHitPosition())
        {
            SceneView.RepaintAll();
        }

        RecalculateMarkerPosition();

        Event current = Event.current;

        if (IsMouseOnLayer())
        {
            if (current.type == EventType.MouseDown || current.type == EventType.MouseDrag)
            {
                // right click
                if (current.button == 1)
                {
                    Erase();
                    current.Use();
                }
                // left click
                else if (current.button == 0)
                {
                    Draw();
                    current.Use();
                }
            }
        }

        // UI tip for how to draw/erase tiles
        Handles.BeginGUI();
        GUI.Label(new Rect(10, Screen.height - 90, 100, 100), "LMB: Draw");
        GUI.Label(new Rect(10, Screen.height - 105, 100, 100), "RMB: Erase");
        Handles.EndGUI();
    }

    public override void OnInspectorGUI()
    {
        TileMap map = (TileMap)target;

        //serializedObject.Update();

        GUILayout.Label("TileMap Properties", EditorStyles.boldLabel);
        map.properties.rows = EditorGUILayout.IntField("Rows: ", map.properties.rows);
        map.properties.columns = EditorGUILayout.IntField("Columns: ", map.properties.columns);
        map.properties.tileWidth = EditorGUILayout.FloatField("Tile Width: ", map.properties.tileWidth);
        map.properties.tileHeight = EditorGUILayout.FloatField("Tile Height: ", map.properties.tileHeight);

        GUILayout.Label("Tile Creation Properties", EditorStyles.boldLabel);
        placement = (PlacementState)EditorGUILayout.EnumPopup("Placement Mode: ", placement);
        tileObj = (GameObject)EditorGUILayout.ObjectField("Obj", tileObj, typeof(GameObject), false);

        if (tileObj?.GetComponent<Tile>() == null)
        {
            GUILayout.Label("!!! Selected Prefab is NOT a Tile !!!", EditorStyles.boldLabel);
        }
    }

    private void OnEnable()
    {
        // Set the current tool to the view tool
        Tools.current = Tool.View;
        Tools.viewTool = ViewTool.FPS;

        editors.Add(this);
    }

    private void OnDisable()
    {
        editors.Remove(this);
    }

    // Member Methods
    private void Draw()
    {
        TileMap map = (TileMap)target;

        // Calculate mouse position over the tile layer
        Vector3 tilePos = GetTilePositionFromMouseLocation();

        // Check to see if a tile has already been created at mouse position
        //GameObject obj = GameObject.Find(string.Format("Tile_{0}_{1}", tilePos.x, tilePos.y));
        string tileNamePos = $"Tile_{tilePos.x}_{tilePos.y}";
        string tileNameFull = $"Tile_{tilePos.x}_{tilePos.y}_{tileObj?.name}";

        GameObject obj = null;

        for (int ii = map.transform.childCount-1; ii >= 0; ii--)
        {
            Transform child = map.transform.GetChild(ii);
            obj = child.name.Contains(tileNamePos) ? child.gameObject : null;

            if (obj != null)
            {
                if (placement == PlacementState.Overwrite) DestroyImmediate(obj);
                else break;
            }
        }

        // check if the obj is a child of the map
        if (obj != null)
        {
            if (obj.transform.parent != map.transform) return;

            if (placement == PlacementState.Normal) return;
        }

        // if no obj was found, create one
        if (obj == null || placement == PlacementState.Overlap)
        {
            obj = (tileObj == null) ? GameObject.CreatePrimitive(PrimitiveType.Cube) : Instantiate(tileObj);
        }

        // set obj position
        Vector3 tilePosLocal = new Vector3((tilePos.x * map.properties.tileWidth) + (map.properties.tileWidth / 2),
                                           (tilePos.y * map.properties.tileHeight) + (map.properties.tileHeight / 2));
        obj.transform.position = map.transform.position + tilePosLocal;

        // scale the obj
        obj.transform.localScale = new Vector3(map.properties.tileWidth, map.properties.tileHeight, 1);

        // set the parent
        obj.transform.parent = map.transform;

        // set the name for organization
        obj.name = tileNameFull;
    }
    private void Erase()
    {
        TileMap map = (TileMap)target;

        Vector3 tilePos = GetTilePositionFromMouseLocation();

        GameObject cube = GameObject.Find($"Tile_{tilePos.x}_{tilePos.y}");

        if (cube != null && cube.transform.parent == map.transform)
        {
            DestroyImmediate(cube);
        }
    }

    // Helpers
    private Vector2 GetTilePositionFromMouseLocation()
    {
        TileMap map = (TileMap)target;

        Vector3 pos = new Vector3(mouseHitPos.x / map.properties.tileWidth,
                                  mouseHitPos.y / map.properties.tileHeight,
                                  map.transform.position.z);

        // round each number using 5 decimal place precision
        pos = new Vector3((int)System.Math.Round(pos.x, 5, System.MidpointRounding.ToEven),
                          (int)System.Math.Round(pos.y, 5, System.MidpointRounding.ToEven));

        // check to ensure that the row and column are within the bounds
        int col = (int)pos.x;
        int row = (int)pos.y;
        col = Mathf.Clamp(col, 0, map.properties.columns - 1);
        row = Mathf.Clamp(row, 0, map.properties.rows - 1);

        return new Vector2(col, row);
    }
    private bool IsMouseOnLayer()
    {
        TileMap map = (TileMap)target;

        return mouseHitPos.x > 0 && mouseHitPos.x < (map.properties.columns * map.properties.tileWidth) &&
               mouseHitPos.y > 0 && mouseHitPos.y < (map.properties.rows * map.properties.tileHeight);
    }
    private void RecalculateMarkerPosition()
    {
        TileMap map = (TileMap)target;

        Vector3 tilePos = GetTilePositionFromMouseLocation();

        // store the world space tile position
        Vector3 worldPos = new Vector3(tilePos.x * map.properties.tileWidth, tilePos.y * map.properties.tileHeight);
        worldPos.x += map.properties.tileWidth / 2;
        worldPos.y += map.properties.tileHeight / 2;
        // set the marker position
        map.markerPosition = map.transform.position + worldPos;
    }
    private bool UpdateHitPosition()
    {
        TileMap map = (TileMap)target;

        // build a plane object
        Plane p = new Plane(map.transform.TransformDirection(Vector3.forward), map.transform.position);

        // build a ray from mouse position
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

        Vector3 hit = new Vector3();
        float dist;

        if (p.Raycast(ray, out dist))
        {
            hit = ray.origin + (ray.direction.normalized * dist);
        }

        // convert hit location into local space
        Vector3 localHit = map.transform.InverseTransformPoint(hit);

        // if new hit is different, set stored location and update
        if (localHit != mouseHitPos)
        {
            mouseHitPos = localHit;
            return true;
        }

        return false;
    }
}
