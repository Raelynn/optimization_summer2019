﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TileMapWindow : EditorWindow
{
    TileMapProperties properties;

    private bool advancedOptionsEnabled = false;

    [MenuItem("Tools/Create OR Edit Tile Map")]
    static void Init()
    {
        // Get existing open window, or make a new one
        TileMapWindow window = EditorWindow.GetWindow<TileMapWindow>();
        //window.GetCreateTileMap();
        window.Show();
    }

    private void OnGUI()
    {
        //GUILayout
        //EditorGUILayout
        //if (!editor) editor = TileMapEditor.editors.Where(e => e.target == map).Take(1).ToArray()[0];

        GUILayout.Label("Tile Map Editor", EditorStyles.boldLabel);

        properties.rows = EditorGUILayout.IntField("Rows: ", properties.rows);
        properties.columns = EditorGUILayout.IntField("Columns: ", properties.columns);
        properties.tileWidth = EditorGUILayout.FloatField("Tile Width: ", properties.tileWidth);
        properties.tileHeight = EditorGUILayout.FloatField("Tile Height: ", properties.tileHeight);

        advancedOptionsEnabled = EditorGUILayout.BeginToggleGroup("Advanced Options", advancedOptionsEnabled);

        EditorGUILayout.EndToggleGroup();

        if (GUILayout.Button("Create TileMap"))
        {
            CreateTileMap();
            this.Close();
        }
    }

    //private void GetCreateTileMap()
    //{
    //    TileMap m = Selection.activeGameObject?.GetComponent<TileMap>();
    //    if (m == null)
    //    {
    //        m = new GameObject("TileMap").AddComponent<TileMap>();
    //        Selection.activeGameObject = m.gameObject;
    //    }
    //    map = m;
    //    editor = TileMapEditor.editors.Where(e => e.target == map).Take(1).ToArray()[0];
    //}

    private void CreateTileMap()
    {
        TileMap m = new GameObject("TileMap").AddComponent<TileMap>();
        m.SetProperties(properties);
        Selection.activeGameObject = m.gameObject;
    }
}
