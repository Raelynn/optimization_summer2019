﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct TileMapProperties
{
    public int rows;
    public int columns;
    public float tileWidth;
    public float tileHeight;
}

public class TileMap : MonoBehaviour
{
    public TileMapProperties properties;

    // Used by editor components or game logic to indicate a tile location
    [HideInInspector]
    public Vector3 markerPosition;

    public TileMap()
    {
        properties.rows = 20;
        properties.columns = 10;

        //Tile t1 = new Tile("tileOne");
        //Tile t2 = t1;

        //t2.name = "tileTwo";

        //Debug.Log($"t1: {t1.name}"); // tileTwo
        //Debug.Log($"t2: {t2.name}"); // tileTwo

        //markerPosition.x = 10;
        //Vector3 other = markerPosition;
        //other.x = 15;

        //Debug.Log($"markerPosition.x: {markerPosition.x}"); // 10
        //Debug.Log($"other.x: {other.x}");                   // 15
    }

    // When the game object with this script is selected, draw a grid
    private void OnDrawGizmosSelected()
    {
        // store map width, height, & position
        float mapWidth = properties.columns * properties.tileWidth;
        float mapHeight = properties.rows * properties.tileHeight;
        Vector3 pos = transform.position;

        // draw layer border
        Gizmos.color = Color.white;
        Gizmos.DrawLine(pos, pos + new Vector3(mapWidth, 0));
        Gizmos.DrawLine(pos, pos + new Vector3(0, mapHeight));
        Gizmos.DrawLine(pos + new Vector3(mapWidth, 0), pos + new Vector3(mapWidth, mapHeight));
        Gizmos.DrawLine(pos + new Vector3(0, mapHeight), pos + new Vector3(mapWidth, mapHeight));

        // draw tile cells
        Gizmos.color = Color.grey;
        for(int ii = 1; ii < properties.columns; ii++)
        {
            Gizmos.DrawLine(pos + new Vector3(properties.tileWidth * ii, 0),
                            pos + new Vector3(properties.tileWidth * ii, mapHeight));
        }
        for (int ii = 1; ii < properties.rows; ii++)
        {
            Gizmos.DrawLine(pos + new Vector3(0, properties.tileHeight * ii),
                            pos + new Vector3(mapWidth, properties.tileHeight * ii));
        }

        // draw marker position
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(markerPosition, new Vector3(properties.tileWidth, properties.tileHeight, 1) * 1.1f);
        
    }

    public void SetProperties(TileMapProperties newProps)
    {
        properties.rows = newProps.rows;
        properties.columns = newProps.columns;
        properties.tileWidth = newProps.tileWidth;
        properties.tileHeight = newProps.tileHeight;
    }
}
