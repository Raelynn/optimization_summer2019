﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_derived : test_base
{
    protected int randOne = 5;
    protected float randTwo = 8.75f;
    protected Vector3 randThree = new Vector3(1.0f, 2.0f, 3.0f);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    protected override void Foo()
    {
        Debug.Log("Running from derived");
    }

    public override bool Equals(object other)
    {
        // Check for null & compare run-time types
        // returns true only if other is of type test_derived
        bool isType_specific1 = this.GetType().Equals(other.GetType());
        bool isType_specific2 = other.GetType() == typeof(test_derived);
        // return true if other is of type test_derived OR test_base
        bool isType_family = (other is test_derived);
        if ((other == null) || !isType_family)
        {
            return false;
        }
        else
        {
            test_derived t = (test_derived)other;
            return (randOne == t.randOne) && (randTwo == t.randTwo) && (randThree.Equals(t.randThree));
        }
    }

    public static test_derived operator + (test_derived lhs, test_derived rhs)
    {
        test_derived ret = new test_derived();
        ret.randOne = lhs.randOne + rhs.randOne;
        ret.randTwo = lhs.randTwo + rhs.randTwo;
        ret.randThree = lhs.randThree + rhs.randThree;
        return ret;
    }


}
