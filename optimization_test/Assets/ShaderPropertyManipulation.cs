﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderPropertyManipulation : MonoBehaviour
{
    public float effectTime = 1.0f;
    private float effectTimer = 0.0f;
    private bool inflating = true;

    private float maxInflateAmt = 0.02f;

    private Renderer rend;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (inflating) Inflate();
        else Deflate();

        float inflation = maxInflateAmt * effectTimer;
        rend.material.SetFloat("_Amount", inflation);
        
    }

    void Inflate()
    {
        effectTimer += Time.deltaTime;
        if (effectTimer >= effectTime)
        {
            inflating = false;
        }
    }
    void Deflate()
    {
        effectTimer -= Time.deltaTime;
        if (effectTimer <= -effectTime)
        {
            inflating = true;
        }
    }
}
