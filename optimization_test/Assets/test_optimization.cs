﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_optimization : MonoBehaviour
{
    public test_derived t1;
    public test_derived t2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bool equal = t1.Equals(t2);

        test_derived result = t1 + t2;
        t1 += t2;
    }

    protected virtual void Foo()
    {
        Debug.Log("Running from base");
    }

}
