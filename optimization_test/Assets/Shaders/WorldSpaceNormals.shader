﻿Shader "Unlit/WorldSpaceNormals"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
				half3 worldNormal : TEXCOORD0;
                float4 pos : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (float4 vertex : POSITION, float3 normal : NORMAL)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(vertex);
				o.worldNormal = UnityObjectToWorldNormal(normal);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 col = 0;
				// Normal is a 3D vector with xyz in -1..1 range
				// To display as a color, bring the range in 0..1
				// and convert into red, green, blue components
				col.rgb = i.worldNormal * 0.5 + 0.5;
				return col;
            }
            ENDCG
        }
    }
}
