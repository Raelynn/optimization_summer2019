﻿Shader "Unlit/SingleColour"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color("Main Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            float4 vert (float4 vertex: POSITION) : SV_POSITION
            {
                return UnityObjectToClipPos(vertex);
            }

			// color from material setting
			fixed4 _Color;

			// pixel shader, no inputs needed
            fixed4 frag () : SV_Target
            {
                return _Color;
            }
            ENDCG
        }
    }
}
