﻿Shader "Unlit/Triplanar"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Tiling ("Tiling", Float) = 1.0
		_OcclusionMap("Occlusion", 2D) = "white" {}
    }
    SubShader
    {
        //Tags { "RenderType"="Opaque" }
        //LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct v2f
            {
				half3 objNormal : TEXCOORD0;
				float3 coords : TEXCOORD1;
                float2 uv : TEXCOORD2;
                float4 vertex : SV_POSITION;
            };

			float _Tiling;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.coords = v.vertex.xyz * _Tiling;
				o.objNormal = v.normal;
				o.uv = v.uv;
                return o;
            }

			sampler2D _MainTex;
			sampler2D _OcclusionMap;

            fixed4 frag (v2f i) : SV_Target
            {
                // use the absolute value of normal as texture weights
				half3 blend = abs(i.objNormal);
				// make sure the weights sum to 1 (divide by sum of x+y+z)
				blend /= (blend.x + blend.y + blend.z);
				// read the texture from its three projection axes
				fixed4 cx = tex2D(_MainTex, i.coords.yz);
				fixed4 cy = tex2D(_MainTex, i.coords.xz);
				fixed4 cz = tex2D(_MainTex, i.coords.xy);
				// blend the textures based on weight
				fixed4 col = (cx * blend.x) + (cy * blend.y) + (cz * blend.z);
				// modulate by the occlusion map
				col *= tex2D(_OcclusionMap, i.uv);
                return col;
            }
            ENDCG
        }
    }
}
